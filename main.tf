module "backend" {
  source = "./modules/api-backend"

  name            = "example-application-backend"
  code_dir        = null
  code_entrypoint = null
  code_runtime    = null
  api_version     = "v1"

  api_methods = {
    message = ["GET"]
    date    = ["GET"]
    secret  = ["POST"]
  }
}

module "frontend" {
  source = "./modules/static-website"

  bucket_name          = "example-application-bucket"
  root_document_object = null
  website_folder       = null
  cache_policy         = null
  domain               = null

  api_backend = {
    origin     = module.backend.origin
    http_port  = 80
    https_port = 443
  }

  providers = {
    aws.virginia = aws.virginia
  }
}