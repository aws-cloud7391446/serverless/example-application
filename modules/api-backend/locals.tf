locals {
  _api_methods = flatten([
    for method_name, http_methods in var.api_methods : [
      for http_method in http_methods : {
        name        = method_name
        http_method = http_method
      }
    ]
  ])
  api_methods = {
    for method in local._api_methods : format("%s-%s", method.name, method.http_method) => {
      name = method.name
      http_method = method.http_method
    }
  }
  api_methods_resources = toset([
    for method_id, method in local.api_methods : method.name
  ])
}