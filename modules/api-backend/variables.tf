variable "name" {
  description = "API backend name. Default is 'api-backend'"
  type        = string
  default     = "api-backend"
  nullable    = false
}

variable "code_dir" {
  description = "Lambda code directory. Default is 'lambda'"
  type        = string
  default     = "lambda"
  nullable    = false
}

variable "code_entrypoint" {
  description = "Lambda code entrypoint. Format is '{file_name}.{entrypoint_function}' Default is 'lambda.lambda_handler'"
  type        = string
  default     = "lambda.lambda_handler"
  nullable    = false
}

variable "code_runtime" {
  description = "Lambda code runtime. Default is 'python3.11'"
  type        = string
  default     = "python3.11"
  nullable    = false
}

variable "api_version" {
  description = "API backend version. Default is 'v1'"
  type        = string
  default     = "v1"
  nullable    = false
}

variable "api_methods" {
  description = "API backend methods"
  type = map(
    set(string)
  )
  default = {
    test = ["GET"]
  }
  nullable    = false
}