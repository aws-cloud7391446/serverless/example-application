locals {
  s3_policy = jsonencode(
    {
        "Version": "2008-10-17",
        "Id": "PolicyForCloudFrontPrivateContent",
        "Statement": [
            {
                "Sid": "AllowCloudFrontServicePrincipal",
                "Effect": "Allow",
                "Principal": {
                    "Service": "cloudfront.amazonaws.com"
                },
                "Action": "s3:GetObject",
                "Resource": "${aws_s3_bucket.s3.arn}/*",
                "Condition": {
                    "StringEquals": {
                        "AWS:SourceArn": "${aws_cloudfront_distribution.s3_distribution.arn}"
                    }
                }
            }
        ]
    }
  )

  api_backend_origin_id   = "api-backend"
  api_backend_origin      = (
    var.api_backend == null ? null :
    trim(var.api_backend.origin, "/")
  )
  api_backend_domain_name = (
    local.api_backend_origin == null ? null :
    join("", slice(split("/", local.api_backend_origin), 0, 1))
  )
  api_backend_origin_path = (
    local.api_backend_origin == null ? null :
    length(split("/", local.api_backend_origin)) == 1 ? null :
    join("/", [""], slice(split("/", local.api_backend_origin), 1, length(split("/", local.api_backend_origin))))
  )

  cache_policy_disabled_id                             = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"
  cache_policy_optimized_id                            = "658327ea-f89d-4fab-a63d-7e88639e58f6"
  origin_request_policy_all_viewer_except_host_header  = "b689b0a8-53d0-40ab-baf2-68738e2966ac"
  origin_request_policy_cors_s3origin                  = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"

  cache_policy_map = {
    disabled  = local.cache_policy_disabled_id
    optimized = local.cache_policy_optimized_id
  }
  cache_policy_id = local.cache_policy_map[var.cache_policy]
}

data "aws_acm_certificate" "domain_certificate" {
  domain   = var.domain.cert
  statuses = ["ISSUED"]

  count    = local.use_domain ? 1 : 0
  provider = aws.virginia
}

resource "aws_cloudfront_origin_access_control" "s3_oac" {
  name                              = aws_s3_bucket.s3.bucket
  description                       = "Website S3 OAC"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  aliases             = local.use_domain ? [var.domain.name] : null
  enabled             = true
  is_ipv6_enabled     = false
  price_class         = "PriceClass_100"
  default_root_object = var.root_document_object

  depends_on          = [aws_s3_object.s3_files]

  origin {
    domain_name              = aws_s3_bucket.s3.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.s3_oac.id
    origin_id                = aws_s3_bucket.s3.bucket
  }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.s3.bucket

    cache_policy_id          = local.cache_policy_id
    origin_request_policy_id = local.origin_request_policy_cors_s3origin

    viewer_protocol_policy = "redirect-to-https"
  }

  dynamic "origin" {
    for_each = var.api_backend != null ? [0] : []

    content {
      domain_name = local.api_backend_domain_name
      origin_id   = local.api_backend_origin_id
      origin_path = local.api_backend_origin_path

      custom_origin_config {
        http_port              = var.api_backend.http_port
        https_port             = var.api_backend.https_port
        origin_protocol_policy = "https-only"
        origin_ssl_protocols   = ["TLSv1.2"]
      }
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.api_backend != null ? [0] : []

    content {
      allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
      cached_methods   = ["GET", "HEAD"]
      target_origin_id = local.api_backend_origin_id
      path_pattern     = "/api/*"

      cache_policy_id          = local.cache_policy_disabled_id
      origin_request_policy_id = local.origin_request_policy_all_viewer_except_host_header

      viewer_protocol_policy = "redirect-to-https"
    }
  }

  restrictions {
    geo_restriction {
      locations        = []
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn            = local.use_domain ? data.aws_acm_certificate.domain_certificate[0].arn : null
    minimum_protocol_version       = local.use_domain ? "TLSv1.2_2021" : null
    ssl_support_method             = local.use_domain ? "sni-only" : null

    cloudfront_default_certificate = local.use_domain ? null : true
  }
}

resource "aws_s3_bucket_policy" "s3_policy" {
  bucket = aws_s3_bucket.s3.id
  policy = local.s3_policy
}