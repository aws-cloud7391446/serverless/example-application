import json
import hashlib

from datetime import datetime


SECRET = 'f3657c3361ea52128ecd6c753bf4dc10d64783a950c788bb0793ed890eab229160d9676dbfc347ee04429ebeeb7726d7f4e38c2e1f7d38d0fc146f4b65c4c3b9'


def method_message(request):
    request.update(msg='Hello from Lambda Serverless Backend!')
    return 200, request

def method_date(request):
    date = str(datetime.now())
    request.update(date=date)

    return 200, request

def method_secret(request, body):
    try:
        data = json.loads(body)
        if 'password' not in data:
            request.update(msg='Found a JSON object but no "password" key!')
            return 400, request

        password = hashlib.sha512(data['password'].encode('UTF-8')).hexdigest()
        if password == SECRET:
            request.update(msg='You found a secret!')
            return 200, request
        else:
            request.update(msg='Wrong password!')
            return 403, request
    except:
        request.update(msg='Something went wrong!')
        return 400, request