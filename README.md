# Example application
Example website application built using the 'static-website' and 'api-backend' projects as modules

## Architecture
![Schema](/uploads/fed32c25cf765c0eba96a9ea496ae15a/schema.png)

Basically two modules used together with one difference in that the CloudFront distribution has two origins,
the first is the S3 Bucket that stores static files for the website and the second is the API Backend with a '/api/*' path pattern
that implements a REST API used by the website

## Website static files origin
Static files were borrowed from [here](https://github.com/Subuthai/VerySimplePortfolio) and adopted to a website use
