const apiUri = "/api/v1/"

const elementDateYearId  = "api-date-year"
const elementDateMonthId = "api-date-month"
const elementDateDayId   = "api-date-day"
const elementDateTimeId  = "api-date-time"

const elementMotdId = "api-motd"

const elementSecretResponseId = "api-secret-response"
const elementSecretInputId    = "api-secret-input"


function apiCall(method, callback) {
    const methodUri = apiUri + method

    fetch(methodUri)
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                throw new Error('API request failed')
            }
        })
        .then(data => {
            callback(data)
        })
        .catch(error => {
            console.error(error)
    })
}


function loadDate() {
    apiCall("date", data => {
        const year  = data.date.split(" ")[0].split("-")[0]
        const month = data.date.split(" ")[0].split("-")[1]
        const day   = data.date.split(" ")[0].split("-")[2]
        const time  = data.date.split(" ")[1].split(".")[0]

        document.getElementById(elementDateYearId).textContent = year
        document.getElementById(elementDateMonthId).textContent = month
        document.getElementById(elementDateDayId).textContent = day
        document.getElementById(elementDateTimeId).textContent = time
    })
}

function loadMotd() {
    apiCall("message", data => {
        const message  = data.msg

        document.getElementById(elementMotdId).textContent = message
    })
}

async function getSecretVideoId(password) {
    const msgBuffer = new TextEncoder().encode(password)
    const hashBuffer = await crypto.subtle.digest('SHA-384', msgBuffer)
    const hashArray = Array.from(new Uint8Array(hashBuffer))
    const hash = hashArray.map(b => b.toString(36).padStart(2, '0')).join('');

    let videoId = ""
    videoId += hash[49].toUpperCase()
    videoId += hash[33].toUpperCase()
    videoId += String.fromCharCode(hash[55].charCodeAt(0) - 2)
    videoId += hash[31].toUpperCase()
    videoId += hash[43].toUpperCase()
    videoId += String.fromCharCode(hash[3].charCodeAt(0) - 1)
    videoId += hash[51].toUpperCase()
    videoId += hash[47].toUpperCase()
    videoId += hash[43]
    videoId += String.fromCharCode(hash[77].charCodeAt(0) + 3).toUpperCase()
    videoId += hash[43]

    return videoId;
}

async function onSendPassword() {
    const methodUri = apiUri + "secret"
    const password = document.getElementById(elementSecretInputId).value
    const body = {"password": password}

    const response = await fetch(methodUri, {
        method: "POST", headers: {"Content-Type": "application/json",}, body: JSON.stringify(body)
    })
    const data = await response.json()

    const responseElement = document.getElementById(elementSecretResponseId)
    responseElement.textContent = data.msg
    if (response.status == 200) {
        responseElement.style.color = "green"
        await new Promise(r => setTimeout(r, 2000));
        const secretVideo = await getSecretVideoId(password)
        location.assign("https://www.youtube.com/watch?v=" + secretVideo);
    } else {
        responseElement.style.color = "red"
    }
}

function hookSecretInputEnter() {
    document.getElementById(elementSecretInputId)
        .addEventListener("keyup", function(event) {
        event.preventDefault()
        if (event.keyCode === 13) {
            onSendPassword()
        }
    });
}


hookSecretInputEnter()
loadDate()
loadMotd()